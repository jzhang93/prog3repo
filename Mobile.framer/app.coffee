# This imports all the layers for "Stuff" into stuffLayers
stuffLayers = Framer.Importer.load "imported/Stuff"
# stuffLayers.visible = true
# stuffLayers.opacity = 0.8

for layerGroupName of stuffLayers
	window[layerGroupName] = stuffLayers[layerGroupName]

#Port7.visible = true
#Port7.opacity = 0.0

Cafes.on Events.Click, ->
	Port6.animate({
		properties: {x: -3000}
		time: .5
	})
	Port7.visible = true
	Port7.x = 1000
	Port7.animate({
		properties: {x: 0}
		time: 0.1
	})
	
Port7.on Events.Click, ->
	Port7.animate({
		properties: {y: -3000}
		time: .5
	})
	Port8.visible = true
	Port8.y = 1000
	Port8.animate({
		properties: {y: 0}
		time: 0.1
	})

Port8.on Events.Click, ->
	Port8.animate({
		properties: {x: -3000}
		time: .5
	})
	Port9.visible = true
	Port9.x = 1000
	Port9.animate({
		properties: {x: 0}
		time: 0.1
	})

Port9.on Events.Click, ->
	Port9.animate({
		properties: {x: -3000}
		time: .5
	})
	Port10.visible = true
	Port10.x = 1000
	Port10.animate({
		properties: {x: 0}
		time: 0.1
	})

Port10.on Events.Click, ->
	Port10.animate({
		properties: {x: -3000}
		time: 0
	})
	Port11.visible = true
	Port11.x = 1000
	Port11.animate({
		properties: {x: 0}
		time: 0
	})

Port11.on Events.Click, ->
	Port11.animate({
		properties: {y: -3000}
		time: 0
	})
	Port12.visible = true
	Port12.y = 3000
	Port12.animate({
		properties: {y: 0}
		time: .5
		delay: 1
	})

Port12.on Events.Click, ->
	Port12.animate({
		properties: {x: -3000}
		time: 0
	})
	Port13.visible = true
	Port13.x = 1000
	Port13.animate({
		properties: {x: 0}
		time: 0
	})

Port13.on Events.Click, ->
	Port13.animate({
		properties: {y: 3000}
		time: 0.2
	})
	Port14.visible = true
	Port14.y = -3000
	Port14.animate({
		properties: {y: 0}
		time: 0.1
	})

# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop

#iconLayer = new Layer width:256, height:256, image:"images/framer-icon.png"
#iconLayer.center()

# Define a set of states with names (the original state is 'default')
#iconLayer.states.add
# 	second: {y:100, scale:0.6, rotationZ:100}
# 	third:  {y:300, scale:1.3, blur:4}
# 	fourth: {y:200, scale:0.9, blur:2, rotationZ:200}

# Set the default animation options
# iconLayer.states.animationOptions =
# 	curve: "spring(500,12,0)"

# # On a click, go to the next state
# iconLayer.on Events.Click, ->
# 	iconLayer.states.next()