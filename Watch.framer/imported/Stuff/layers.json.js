window.__imported__ = window.__imported__ || {};
window.__imported__["Watch/layers.json.js"] = [
  {
    "maskFrame" : null,
    "id" : "FE051402-995C-4AD3-80A2-3AB07CF52D6C",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Port15-FE051402-995C-4AD3-80A2-3AB07CF52D6C.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 333,
        "height" : 379
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 333,
      "height" : 379
    },
    "name" : "Port15"
  },
  {
    "maskFrame" : null,
    "id" : "FA657A29-7B27-47C3-AB65-7AD1212BA0E1",
    "visible" : false,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Port16-FA657A29-7B27-47C3-AB65-7AD1212BA0E1.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 333,
        "height" : 379
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 333,
      "height" : 379
    },
    "name" : "Port16"
  }
]