# This imports all the layers for "Stuff" into stuffLayers
stuffLayers = Framer.Importer.load "imported/Stuff"

# Welcome to Framer

for layerGroupName of stuffLayers
	window[layerGroupName] = stuffLayers[layerGroupName]
	
Port15.on Events.Click, ->
	Port15.animate({
		properties: {x: -3000}
		time: .1
	})
	Port16.visible = true
	Port16.x = 1000
	Port16.animate({
		properties: {x: 0}
		time: 0.1
	})

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop

# iconLayer = new Layer width:256, height:256, image:"images/framer-icon.png"
# iconLayer.center()
# 
# # Define a set of states with names (the original state is 'default')
# iconLayer.states.add
# 	second: {y:100, scale:0.6, rotationZ:100}
# 	third:  {y:300, scale:1.3, blur:4}
# 	fourth: {y:200, scale:0.9, blur:2, rotationZ:200}
# 
# # Set the default animation options
# iconLayer.states.animationOptions =
# 	curve: "spring(500,12,0)"
# 
# # On a click, go to the next state
# iconLayer.on Events.Click, ->
# 	iconLayer.states.next()